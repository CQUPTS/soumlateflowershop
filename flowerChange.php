<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>商品详情页 | Soulmate花店</title>
    <link href="./css/swiper-3.4.2.min.css" rel="stylesheet"/>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/flowerDetail.css" rel="stylesheet"/>
    <style>
      .left{
        padding-top:0; 
      }   
    </style>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li class="active"><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
<!--          <h3 class="title">封面</h3>-->
<!--          <p><img src="./img/pic.png" alt=""/></p>-->
<!--          <p>-->
<!--            <form class="hide" action="replacecover.php" method="post">-->
<!--              <input type="text" name="id" value="12345621345"/>-->
<!--              <input type="text" name="name" value="cover"/>-->
<!--              <input type="file" name="file" id="cover-input" accept="image/*"/>-->
<!--              <input type="submit" id="submit-cover"/>-->
<!--            </form><a href="javascript:;" id="cover-replace">替换封面</a>-->
<!--          </p>-->
            <?php
            include_once ("mysql_conn.php");
            $sql = "SELECT f.*,t.name `type`,p.name `protype` FROM flower f LEFT JOIN ".
             "   type t on t.id = f.typeid LEFT JOIN protype  p on f.protypeid = p.id where f.id = ".$_GET['id'];
            $result = (new mysql_conn())->fetch($sql);
            if (empty($result)){
                echo "<script>alert('暂无改类信息!');window.location='homeAdminItems.php';</script>";

            }else{
            ?>
            <h3 class="title">图片</h3>
            <ul class="photos">

                        <?php
//                        $keywords = preg_split("/;/",$result['photo'] );
                        $photoSql = "select * from photo where fid = ".$result['id'];
                        $photo = (new mysql_conn())->fetchAll($photoSql,array(0,100));
                        if($photo==NULL){
//                            echo  "<div class=\"item-img\"><img src=\"./img/rose999.jpeg\" alt=\"\"/></div> ";
                        }else{
                            foreach ($photo as $row){
                                ?>
                                <li>
                                    <div class="item">
                <?php
//                                echo $row['name'];
                                echo  " <div class=\"item-img\"><img src=\"".$row['addr']."\" alt=\"\"/></div>";
                                ?>

                                <div class="item-text">
                                    <p>
                                    <form class="hide" action="replacephoto.php" method="post">
                                        <input type="text" name="id" value="12345621345"/>
                                        <input type="text" name="name" value="photo1"/>
                                        <input type="file" name="file" accept="image/*"/>
                                        <input type="submit"/>
                                    </form>
<!--                                    <a class="replace" href="flowerChange.php">替换</a>&nbsp;&nbsp;-->
                                    <a class="delete"    href="deletePhoto.php?id=<?php echo $row['id']?>&fid=<?php echo $result['id']?>">删除</a>
                                    </p>
                                </div>
                    </div>
                </li>
                                <?php
                            }
                        }
                        ?>



                <li>
                    <div class="item">
                        <div class="item-img" id="newphotoimg"><img src="./img/new.png" alt=""/></div>
                        <div class="item-text">
                            <p>
                            <form  class="hide" action="upload_file.php" method="post" enctype="multipart/form-data">
<!--                                <form  action="upload_file.php" method="post"  enctype="multipart/form-data">-->

                                <input type="hidden" name="id" value="<?php echo $result['id']?>"/>
<!--                                <input type="hidden" name="photo" value="--><?php //echo $result['photo']?><!--"/>-->
                                <input type="file" name="file" ><br>
                                <input type="submit" >
                            </form>
                            <a href="javascript:;" id="newphoto">新增图片</a>
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
          <div class="right">
              <form action="updateflower.php" method="post">
                  <h1 class="flower-name">
                      <input type="text" name="name" placeholder="请输入商品名称" value="<?php echo $result['name']?>"/>
                  </h1>
                  <h2><a class="flower-protype" href="#" title="品种"><?php echo $result['protype']?></a>·<a class="flower-wrapper" href="#"
                                                                              title="类型"><?php echo $result['type']?></a></h2>
                  <div class="attribute">
                      <table>
                          <tr>
                              <th>花 语：</th>

                              <td>
                                  <input type="text" name="huayu" placeholder="请输入花语" value="<?php echo $result['say']?>"/>
                              </td>
                          </tr>
                          <tr>
                              <th>材 料：</th>
                              <td>
                                  <input type="text" name="cailiao" placeholder="请输入材料" value="<?php echo $result['meta']?>"/>
                              </td>
                          </tr>
                          <tr>
                              <th>备 注：</th>
                              <td>
                                  <input type="text" name="beizhu" placeholder="请输入备注" value="<?php echo $result['remark']?>"/>
                              </td>
                          </tr>
                          <tr>
                              <th>单 价(元)：</th>
                              <td>
                                  <input type="text" name="price" placeholder="请输入单价" value="<?php echo $result['price']?>"/>
                              </td>
                          </tr>
                          <tr class="show-icon">
                              <th>剩余库存：</th>
                              <td>
                                  <input type="text" name="kucun" id="kucun" placeholder="请输入库存量" value="<?php echo $result['number']?>" />
                              </td>
                              <input type="hidden" name="id" value="<?php echo $result['id']?>"/>
                              <input type="hidden" name="option" value="1"/>
                          </tr>
                      </table>
                  </div>
                  <p><a class="btn primary add-to-chart" href="javascript:;" id="newflower-action"><span class="text">修改商品</span></a>
                      <input type="submit" id="true-submit" hidden="hidden"/>
                  </p>
              </form><?php
              }
            ?>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script src="./js/swiper-3.4.2.jquery.min.js"></script>
    <script>
      var galleryTop = new Swiper('.gallery-top', {
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          spaceBetween: 10,
      });
      var galleryThumbs = new Swiper('.gallery-thumbs', {
          spaceBetween: 10,
          centeredSlides: true,
          slidesPerView: 'auto',
          touchRatio: 0.2,
          slideToClickedSlide: true
      });
      galleryTop.params.control = galleryThumbs;
      galleryThumbs.params.control = galleryTop;
      
      $("#kucun").change(function(){
        var kc = $(this).val();
        if(/^\d+$/.test(kc)){
      
        }else{
          $(this).val(1);
        }
      });
      $("#newflower-action").click(function(e){
        $("#true-submit").click();
      })
      $("#cover-replace").click(function(e){
        $("#cover-input").click();
      })
      $('[type="file"]').change(function(e){
          console.log("update")
        $(this).parent("form").submit();
      })
      
      $(".replace").click(function(e){
        $(this).prev().find('[type="file"]').click();
      })
      $("#newphoto").click(function(e){
        $(this).prev().find('[type="file"]').click();
      })
      $("#newphotoimg").click(function(){
          $("#newphoto").click();
      })
    </script>
  </body>
</html>