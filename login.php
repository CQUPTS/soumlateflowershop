<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>登录|soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/login.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="login-box">
        <h1 class="title font-flower">Soulmate</h1>
          <?php
          if (isset( $_COOKIE['php'])){
              echo $_COOKIE['php'];
          }else{

          }
          ?>
        <form action="login_v.php" method="post">
          <table>
            <tr>
              <th>用户名:</th>
              <td> 
                <input type="text" name="username" placeholder="请输入您的用户名"/>
              </td>
            </tr>
            <tr>
              <th>密码:</th>
              <td> 
                <input type="password" name="password" placeholder="请输入您的密码"/>
              </td>
            </tr>
          </table>
          <p><a class="primary btn" href="javascript:;" id="js-login"><span class="text">登录</span></a></p>
          <input type="submit" id="true-submit" hidden="hidden"/>
          <p class="ps-info">还没有账号？<a href="regist.php">前往注册</a></p>
        </form>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $('#js-login').on("click",function(){
        $("#true-submit").click();
      })
        
    </script>
  </body>
</html>