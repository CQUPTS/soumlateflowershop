<?php


class mysql_conn
{
    public function getConn(){
        $pdo = new PDO('mysql:host=localhost;dbname=soumlate_db;port=3306','root','root');
        $pdo->exec('set names utf8');
        return $pdo;
    }
    /*插入\更新数据*/
    public function add_upadate($sql){
        $pdo = $this->getConn();
        $pdo->query($sql);
        return true;
    }
    public function fetchAll($sql, $limit = array(0, 10), $dataMode = PDO::FETCH_ASSOC, $preType = array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)) {
        if ($sql) {
            $sql .= ' limit ' . (int) $limit[0] . ',' . (intval($limit[1]) > 0 ? intval($limit[1]) : 10);
            $pdo = $this->getConn();
            $pdoStatement = $pdo->prepare($sql, $preType);
            $pdoStatement->execute();
            return $data = $pdoStatement->fetchAll($dataMode);
        } else {
            return false;
        }
    }
    public function fetch($sql, $searchData = array(), $dataMode = PDO::FETCH_ASSOC, $preType = array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)) {
        if ($sql) {
            $sql .= ' limit 1';
            $pdo = $this->getConn();
            $pdoStatement = $pdo->prepare($sql, $preType);
            $pdoStatement->execute($searchData);
            return $data = $pdoStatement->fetch($dataMode);
        } else {
            return false;
        }
    }
    /*查询数据*/
    public function select($sql){
        $pdo = $this->getConn();
        $res=$pdo->prepare($sql);//准备查询语句
        $res->execute();
        $result=$res->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}