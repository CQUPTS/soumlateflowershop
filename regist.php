<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>登录|soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/login.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="login-box">
        <h1 class="title font-flower">Soulmate</h1>
          <?php


          ?>
        <form action="mysql_connection.php" method="post">
          <table>
            <tr>
              <th>用户名:</th>
              <td> 
                <input type="text" name="username" placeholder="请填写您的用户名"/>
              </td>
            </tr>
            <tr>
              <th>密码:</th>
              <td> 
                <input type="password" name="password" placeholder="请填写您的密码"/>
              </td>
            </tr>
            <tr>
              <th>确认密码:</th>
              <td> 
                <input type="password" name="password2" placeholder="请再次确认您的密码"/>
              </td>
            </tr>
            <tr>
              <th>怎么称呼您:</th>
              <td> 
                <input type="text" name="name" placeholder="请填写您的姓名"/>
              </td>
            </tr>
            <tr>
              <th>性别:</th>
              <td> 
                <input type="radio" name="sex" value="man" checked="checked"/>男
                <input type="radio" name="sex" value="woman"/>女
              </td>
            </tr>
            <tr>
              <th>联系电话:</th>
              <td> 
                <input type="text" name="phone" placeholder="请填写您的电话号码"/>
              </td>
            </tr>
            <tr>
              <th>联系地址:</th>
              <td> 
                <input type="text" name="location" placeholder="请填写您的详细地址"/>
              </td>
            </tr>
          </table>
          <p><a class="primary btn" href="javascript:;" id="js-regist"><span class="text">注册</span></a>
            <input type="submit" id="true-submit"" hidden="hidden"/>
          </p>
          <p class="ps-info">已有账号？<a href="login.php">马上登录</a></p>
        </form>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $("#js-regist").on("click",function(){
        $("#true-submit").click();
      })
                
    </script>
  </body>
</html>