<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>浏览 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/flower.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li class="active"><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();

            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="index">
          <h3>按类别</h3>
          <ul>
              <li><a href="flower.php?tid=0">所有品种</a></li>
              <?php
              error_reporting(0);
              include_once ("mysql_conn.php");
              $tid = $_GET['tid'];
              $pid = $_GET['pid'];
              $name = $_GET['name'];
              $conn = new mysql_conn();
              $row= $conn->fetchAll("select * from type");
              $protype=NULL;
              foreach($row  as $value) {
                    if (!empty($tid)&&$tid!=0){
                        if ($value['id']==$tid){
                            echo "<li class=\"active\"><a href=\"flower.php?tid=" . $value['id'] . "\">" . $value['name'] . "</a></li>";
                            $protype="品种:".$value['name'];
                        }else{
                            echo "<li><a href=\"flower.php?tid=" . $value['id'] . "\">" . $value['name'] . "</a></li>";
                        }
                    }else {
                        echo "<li><a href=\"flower.php?tid=" . $value['id'] . "\">" . $value['name'] . "</a></li>";
                    }

              }
              ?>


          </ul>
          <h3>按品种</h3>
          <ul>
              <li><a href="flower.php?pid=0">所有品种</a></li>
              <?php
              include_once ("mysql_conn.php");
              $conn = new mysql_conn();
              $row= $conn->fetchAll("select * from protype");
              $type=NULL;
              foreach($row  as $value) {
                  if (!empty($pid)&&$pid!=0){
                      if ($value['id']==$pid){
                          echo "<li class=\"active\"><a href=\"flower.php?pid=".$value['id']."\">" . $value['name'] . "</a></li>";
                          $type="类别：".$value['name'];
                      }else{
                          echo "<li><a href=\"flower.php?pid=".$value['id']."\">" . $value['name'] . "</a></li>";
                      }
                  }else {
                      echo "<li><a href=\"flower.php?pid=".$value['id']."\">" . $value['name'] . "</a></li>";
                  }
              }
//              $anwser = empty($protype)?empty($type)?empty($name)?"搜索\"关键词\"结果":"搜索  ".$name." 结果":$type:$protype;
              $anwser =empty($name)?NULL:"搜索  ".$name." 结果";
              ?>


          </ul>
        </div>
        <div class="content">
          <p>您的当前位置：<a href="./flower.php">全部</a>
            <?php if(!empty($anwser)){
                echo ">".$anwser;
            }?><span class="search">
              <input type="text" name="name"placeholder="请输入关键词" id="search_text"/><a href="javascript:;" id="search">搜索</a></span>
          </p>
          <ul class="productitems">
              <?php


              if (!empty($tid)&&$tid!=0) {
                  $sql = "select * from flower where typeid=".$tid." order by id desc";
              } else if(!empty($pid)&&$pid!=0){
                  $sql = "select * from flower where protypeid=".$pid." order by id desc";
              }else if(!empty($name)){
                  $sql = "select * from flower where  name like '%".$name."%' order by id desc";
                  echo $sql;
              }else{
                  $sql = "select * from flower order by id desc ";
              }

              $row= $conn->fetchAll($sql,array(0, 100));
              foreach($row  as $value) {
                  $photo=(new mysql_conn())->fetch("select * from photo where fid = ".$value['id']);
                  $photo =( $photo== NULL ? "./img/rose999.jpeg" :"./". $photo['addr']);
                  ?>
                  <li>

                      <div class="item">
                          <div class="item-img"><img src="<?php echo $photo?>" alt=""/></div>
                          <div class="item-text"><a class="item-name" href="<?php echo "flowerDetail.php?fid=".$value['id']?>"
                                                    target="_blank"><?php echo $value['name']?></a>
                              <p class="item-price"><?php echo $value['price']?></p>
                          </div>
                      </div>
                  </li>
                  <?php
              }
              ?>

          </ul>
          <div class="pagination">
<!--            <p> <a href="#">上一页</a><a href="#">1</a><a class="active" href="#">2</a><a href="#">3</a><a href="#">...</a><a href="#">9</a><a href="#">10</a><a href="#">下一页</a></p>-->
          </div>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $("#search").on("click",function(){
        var search_text = $('#search_text').val();
        window.location.href = "./flower.php?name=" + encodeURI(search_text);
      });
       $('.productitems').on("click",'a',function(e){
        e.cancelBubble=true;
        e.stopPropagation();
       });
      $('.productitems').on("click",'.item',function(e){
         $(this).find('a')[0].click();
        e.cancelBubble=true;
        e.preventDefault();
        e.stopPropagation();
        return false;
      })
        
    </script>
  </body>
</html>