<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>个人中心 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['user'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="homeCustomer.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
          <h3 class="title">个人中心</h3>
          <ul>
            <li><a href="homeAdminOrders.php">管理订单</a></li>
            <li><a href="homeAdminProtypes.php">品种管理</a></li>
            <li class="active"><a href="homeAdminTypes.php">类别管理</a></li>
            <li><a href="homeAdminItems.php">商品管理</a></li>
            <li><a href="homeAdminProfile.php">店铺信息</a></li>
            <li><a href="homeAdminSetting.php">账户设置</a></li>
          </ul>
        </div>
        <div class="right">
          <h1 class="title">所有类别</h1>
          <hr/>
          <ul>
              <?php
              include_once ("mysql_conn.php");
              $conn = new mysql_conn();
              $row= $conn->fetchAll("select * from type;");
              foreach($row  as $value) {
                  echo "<li class=\"typesitem\"><span class=\"name\" data-id=\"".$value['id']."\">".$value['name']."".
                      "</span><span class=\"actions\"><a class=\"update\" href=\"javascript:;\">修改</a><a class=\"delete\" href=\"deletetypename.php?id=".$value['id']."\">删除</a></span></li>
          ";
              }
              ?>
            <li class="typesitem"><a id="new-type" href="javascript:;">新增</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $(".update").click(function(e){
        var nod = $(e.target).parent().prev();
        var name = nod.text();
        var id = nod.data("id");
        var name2= prompt("修改为",name);
        if(name2){
          window.location.href="updatetypename.php?id="+id+"&name="+name2;
        }
      });
      $("#new-type").click(function(){
        var name2= prompt("请输入新的种类的名称");
        if(name2){
          window.location.href="addtypename.php?name="+name2;
        }
      })
      $("#delete").click(function(){
          var id = nod.data("id");
              window.location.href="deletetypename.php?id="+id;
      })
                
        
    </script>
  </body>
</html>