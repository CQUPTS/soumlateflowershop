<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>个人中心 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['id'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="homeCustomer.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
          <h3 class="title">个人中心</h3>
          <ul>
            <li class="active"><a href="homeCustomer.php">基本信息</a></li>
            <li><a href="homeCustomerOrders.php">管理订单</a></li>
            <li><a href="homeCustomerSetting.php">账号设置</a></li>
<!--            <li><a href="homeAdminOrders.php">店主个人中心</a></li>-->
          </ul>
        </div>
        <div class="right">
          <h1> <span>

                  <?php
                  include_once("mysql_conn.php");
                  $conn = new mysql_conn();
                  $sql = "select * from user where id = " . $_SESSION['id'] ;
                  $result = $conn->select($sql);

                  echo  $result['username']==NULL?" " :$result['username'];
                  ?></span>&nbsp;先生您好~</h1>
          <form action="updateCustomer.php" method="post">
            <table>

              <tr>
                <th>默认联系人：</th>
                <td> 
                  <input type="text" name="name" placeholder="请输入默认联系人" value="<?php

                  echo  $result['name']==NULL?" " :$result['name'];

                  ?>"/>
                    <input type="hidden" name="id" placeholder="请输入默认联系人" value="<?php

                    echo  $result['id'];

                    ?>"/>
                </td>
              </tr>
              <tr>
                <th>联系电话：</th>
                <td>
                  <input type="text" name= "phone" placeholder="请输入联系电话" value="<?php

                  echo  $result['phone']==NULL?" " :$result['phone'];

                  ?>"/>
                </td>
              </tr>
              <tr>
                    <th>默认地址:</th>
                    <td>
                        <input type="text" name="addr" placeholder="请输入默认地址" value="<?php

                        echo  $result['addr']==NULL?" " :$result['addr'];

                        ?>"/>
                    </td>
                </tr>
            </table><a class="btn primary" href="javascript:;" id="save-action"><span class="text">保存信息</span></a>
            <input type="submit" id="true-submit" hidden="hidden"/>
          </form>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $("#save-action").on("click",function(){
        $("#true-submit").click();
      })
                
        
    </script>
  </body>
</html>