<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>个人中心 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['user'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="homeCustomer.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
          <h3 class="title">个人中心</h3>
          <ul>
            <li><a href="homeCustomer.php">基本信息</a></li>
            <li><a href="homeCustomerOrders.php">管理订单</a></li>
            <li class="active"><a href="homeCustomerSetting.php">账号设置</a></li>
          </ul>
        </div>
        <div class="right">
          <form action="updatepwd.php" method="post">
            <h1>修改密码</h1>
            <p> <span class="placeholder">原密码:</span>
              <input type="password" name="oldpassword" placeholder="请输入您的原密码"/>
            </p>
            <p> <span class="placeholder">新密码:</span>
              <input type="password" name="oldpassword" placeholder="请输入您的新密码"/>
            </p>
            <p> <span class="placeholder">再次确认密码:</span>
              <input type="password" name="newpassword" placeholder="请再次确认您的新密码"/>
            </p>
            <p><a class="btn primary" href="javascript:;" id="update-pwd"> <span class="text">更新密码</span></a>
              <input type="submit" id="true-submit" hidden="hidden"/>
            </p>
          </form>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $("#update-pwd").on("click",function(){
        $("#true-submit").click();
      })
                
        
    </script>
  </body>
</html>