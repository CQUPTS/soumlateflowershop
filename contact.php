<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>联系方式 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <style>
      .main{width:600px;}   
      table{
        width:100%;
      }   
      [class^="icon-"]:before {
          content: "";
          display: inline-block;
          margin-right:1.3em;
          width: 2em;
          height: 2em;
          vertical-align: middle;
          background-size: cover;
          background-position: center center;
      }
      
      .icon-weixin:before {
          background-image: url('./img/wechat-b.png');
      }
      
      .icon-qq:before {
          background-image: url('./img/qq-b.png');
      }
      
      .icon-phone:before {
          background-image: url('./img/phone-b.png');
      }
      
      .icon-weibo:before {
          background-image: url('./img/weibo-b.png');
      }
      
      .icon-location:before {
          background-image: url('./img/location-b.png');
      }
      
    </style>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li class="active"><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <h1>联系方式</h1>
        <table>
          <tr>
            <td> <i class="icon-phone"></i>1888888888</td>
            <td><i class="icon-weixin"></i>wxsoulmate073</td>
          </tr>
          <tr>
            <td> <i class="icon-qq"></i>782423535</td>
            <td> <i class="icon-weibo"></i>soulmate花店</td>
          </tr>
        </table>
        <h1>详细地址</h1>
        <p> <i class="icon-location"></i>重庆市南岸区崇文路520号</p>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
  </body>
</html>