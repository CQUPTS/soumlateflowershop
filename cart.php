<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>购物车 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/cart.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['user'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }else{
      if ($_SESSION['perssime']==1){
          echo "<script language='javascript'>alert('您是管理员不能进入购物车！')</script>";
          echo "<script>window.location='index.php';</script>";
      }
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li class="active"><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <form action="checkcart.php" method="post">
            <?php
            include_once ("mysql_conn.php");
            $result = (new mysql_conn())->fetch("select s.id,f.id fid,f.name name,f.price price,s.number".
                " from flower f ,user u, shopcart s where u.id = ".$_SESSION['id']." and u.id = s.userid and f.id = s.flowerid and s.flag = 1 ;");
            if ($result == NULL){
                echo "还没有购买任何商品";
                ?>
                <p class="center"><a class="primary btn" href="flower.php" id="buy"> <span class="text">前往选购</span></a>
                    <input type="submit" id="true-submit" hidden="hidden"/> </p>
                <?php
            }else {
                ?>
          <table>
            <thead>
              <tr>
                <th>商品名称</th>
                <th>价格</th>
                <th>数量</th>
                <th>操作</th>
              </tr>
            </thead>

            <tbody>
              <tr>

                      <td><?php echo $result['name']?></td>
                      <td>&yen;<span class="price"><?php echo $result['price']?></span></td>
                      <td><a class="icon-minus" href="javascript:;">-</a>
                          <input type="hidden" name="sid" value="<?php echo $result['id']?>"/>
                          <input class="input-number" type="text" value="<?php echo $result['number']?>" name="number"/><a
                                  class="icon-plus" href="javascript:;">+</a>
                      </td>
                      <td><a href="deleteCart.php?id=<?php echo $result['id']?>">删除</a></td>

              </tr>
            </tbody>
          </table>
          <h3 class="confirm">合计：<span class="total-money">&yen;    <?php
                  echo $result['price']*$result['number'];

                  ?></span></h3>
            <p class="center"><a class="primary btn" href="javascript:;" id="buy"> <span class="text">确认购买</span></a>
                <input type="submit" id="true-submit" hidden="hidden"/> </p>
                <?php
            }
            ?>



        </form>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
       $(".icon-plus").on("click",function(){
        var val = Number($(".input-number").val());
        $(".input-number").val(val+1);
         count();
      })
      $(".icon-minus").on("click",function(){
        var val = Number($(".input-number").val());
        if(val>1){
          $(".input-number").val(val-1);
          count();
        }
      })
      $(".input-number").on("input",function(){
        var val = $(".input-number").val();
        if(/^\d+$/.test(val)){
            if(Number(val)<1){
              val =1;
              $(".input-number").val(1);
            }
        }else{
          val =1;
          $(".input-number").val(1);
        }

       count();

      });
      function count(){
        var total = 0;
        var list =  $("tbody tr");
        for(var i =0 ;i<list.length;i++){
          var price = Number($(list[i]).find(".price").text());
          var number = Number($(list[i]).find(".input-number").val());
          console.log(price,number);
          total+=price*number;
        }
        $(".total-money").html("&yen;"+total.toFixed(2));
      }
      $("#buy").click(function(){
          $("#true-submit").click();
      })
        
    </script>
  </body>
</html>