<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>订单详情 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
<?php
session_start();
if(empty($_SESSION['user'])){
    echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
    setcookie('message',"请登录");
    echo "<script>window.location='checkrose.php';</script>";
}?>

    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main order-detail">
        <h1 class="title center">订单详情</h1>
          <?php
            include_once ("mysql_conn.php");
            $result = (new mysql_conn())->fetch("select o.id,o.date,o.addr,o.status,o.`name` givename,o.phone,f.`name`,f.price,f.photo,s.number from `order` o ,shopcart s,flower f where o.cartid = s.id and f.id = s.flowerid  and o.id =".$_GET['id']);
            if (empty($result)){
                echo "输入错误";
                echo "select o.id,o.date,o.addr,o.status,o.`name` givename,o.phone,f.`name`,f.price,f.photo,s.number from `order` o ,shopcart s,flower f where o.cartid = s.id and f.id = s.flowerid  and o.id =".$_GET['id'];
            }else {
                $photo=strtok($result['photo'],";")==NULL?"./img/rose999.jpeg":strtok($result['photo'],";");
                ?>
                <table>
                    <tr>
                        <th>订单编号：</th>
                        <td><?php echo $result['id']?></td>
                        <th>订单时间：</th>
                        <td><?php echo $result['date']?></td>
                    <tr>
                        <th>联系人:</th>
                        <td><?php echo $result['givename']?></td>
                        <th>联系电话：</th>
                        <td><?php echo $result['phone']?></td>
                    </tr>
                    <tr>
                        <th>收货地址：</th>
                        <td colspan="3"><?php echo $result['addr']?></td>
                    </tr>
                    </tr>
                    <tr>
                        <th>订单物品:</th>
                        <td>
                            <ul>

                                <li>
                                    <div class="item"><span class="item-img"><img src="<?php echo  $photo?>"
                                                                                  alt="图"/></span><span
                                                class="item-name"><?php echo $result['name']?></span><span
                                                class="item-price">&yen;<?php echo $result['price']?></span><span class="item-number">x<?php echo $result['number']?></span>
                                    </div>
                                </li>
                            </ul>
                        <th>合计：</th>
                        <td><span class="total-money">&yen;<?php echo $result['price']*$result['number']?></span></td>
                        </td>
                    </tr>
                    <tr>
                        <!--              <th>是否代送：</th>-->
                        <!--              <td>代送</td>-->
                        <!--              <th>备注：</th>-->
                        <!--              <td>就说感谢那天演讲的时候帮忙救场。</td>-->
                    </tr>
                    </tr>
                </table>
                <?php
            }
          ?>
        <p class="center"><a class="primary btn" href="javascript:window.close();"> <span class="text">关闭本页</span></a></p>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
  </body>
</html>