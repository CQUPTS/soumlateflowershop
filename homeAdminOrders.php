<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>个人中心 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['user'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
          <h3 class="title">个人中心</h3>
          <ul>
            <li class="active"><a href="homeAdminOrders.php">管理订单</a></li>
            <li><a href="homeAdminProtypes.php">品种管理</a></li>
            <li><a href="homeAdminTypes.php">类别管理</a></li>
            <li><a href="homeAdminItems.php">商品管理</a></li>
            <li><a href="homeAdminProfile.php">店铺信息</a></li>
            <li><a href="homeAdminSetting.php">账户设置</a></li>
          </ul>
        </div>
        <div class="right">
          <h1>所有订单 </h1>
            <?php
            include_once ("mysql_conn.php");
            $sql = "select o.id,o.date,o.addr,o.status,f.`name`,f.price,f.photo,s.number from `order` o ,shopcart s,flower f where o.cartid = s.id  and f.id = s.flowerid order by id DESC ";
            $result = (new mysql_conn())->fetchAll($sql,array(0,100));
            if (empty($result)){
                echo "<p>当前没有订单信息</p>";
//                echo "select o.id,o.date,o.addr,o.status,f.`name`,f.price,f.photo,s.number from `order` o ,shopcart s,flower f where o.cartid = s.id  and f.id = s.flowerid  ";

            }else{
            foreach ($result as $row){
//            $photo = strtok($row['photo'], ";") == NULL ? "./img/rose999.jpeg" : strtok($row['photo'], ";");
            $photo=(new mysql_conn())->fetch("select * from photo where fid = ".$row['id']);
            $photo =( $photo== NULL ? "./img/rose999.jpeg" :"./". $photo['addr']);
            ?>
            <ul>
                <li>

                    <div class="order">
                        <p class="order-head"><span class="time"><?php echo $row['date'] ?></span><span
                                    class="uid">订单号：<?php echo $row['id'] ?></span><br/><span
                                    class="location">收货地址：<?php echo $row['addr'] ?></span></p>
                        <table class="order-table">
                            <tr>
                                <td>
                                    <ul>
                                        <li>
                                            <div class="item"><span class="item-img"><img src="<?php echo $photo ?>"
                                                                                          alt="图"/></span><span
                                                        class="item-name"><?php echo $row['name'] ?></span><span
                                                        class="item-price">&yen;<?php echo $row['price'] ?></span><span
                                                        class="item-number">x<?php echo $row['number'] ?></span></div>
                                        </li>
                                    </ul>
                                </td>
                                <td>共计:<span
                                            class="total-money">&yen;<?php echo $row['price'] * $row['number'] ?></span>
                                </td>
                                <td>
                                    <!--                                    根据状态的不同进行不同的显示-->
                                    <?php

                                    if ($row['status'] == 1) {
                                        echo "<p class=\"order-option\"><a class=\"unorder\"  href=\"updateOrder.php?id=" . $row['id'] . "&option=3\">	接收订单</a><br/><a
                                                href=\"orderDetail.php?id=" . $row['id'] . "\" target=\"_blank\">订单详情</a></p>";
//                                    }
//                                    else if ($row['status'] == 2) {
//                                        echo "<p class=\"order-option\"><span>确认收货</span><br/><a
//                                                href=\"orderDetail.php?id=" . $row['id'] . "\" target=\"_blank\">订单详情</a></p>";
                                    } else if ($row['status'] == 4) {
                                        echo "<p class=\"order-option\"><span>交易完成</span><br/><a
                                                href=\"orderDetail.php?id=" . $row['id'] . "\" target=\"_blank\">订单详情</a></p>";
                                    } else if ($row['status'] == 3) {
                                        echo "<p class=\"order-option\"><span>请及时送达</span><br/>
<a class=\"unorder\"  href=\"updateOrder.php?id=" . $row['id'] . "&option=4\">	已送达</a><br/><a
                                                href=\"orderDetail.php?id=" . $row['id'] . "\" target=\"_blank\">订单详情</a></p>";
                                    } else if ($row['status'] == 0) {
                                        echo "<p class=\"order-option\"><span>订单被取消</span><br/><a
                                                href=\"orderDetail.php?id=" . $row['id'] . "\" target=\"_blank\">订单详情</a></p>";
                                    }
                                    ?>
                                    <!--                                    <p class="order-option"><span>-->
                                    <?php //echo
                                    ?><!--</span><br/><a class="unorder"-->
                                    <!--                                                                                       href="退订的url">退订</a><br/><a-->
                                    <!--                                                href="orderDetail.php" target="_blank">订单详情</a></p>-->
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>
                <?php
                }
                }
              ?>
<!--            <li>-->
<!--              <div class="order">-->
<!--                <p class="order-head"><span class="time">2014-07-05 12:37</span><span class="uid">订单号：dd2341245353</span><br/><span class="location">收货地址：玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵</span></p>-->
<!--                <table class="order-table">-->
<!--                  <tr>-->
<!--                    <td>-->
<!--                      <ul>-->
<!--                        <li>-->
<!--                          <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>-->
<!--                        </li>-->
<!--                      </ul>-->
<!--                    </td>-->
<!--                    <td>共计:<span class="total-money">&yen;233</span></td>-->
<!--                    <td>-->
<!--                      <p class="order-option"><a class="accept" href="换成接收的url">接收订单</a><br/><a href="orderDetail.php" target="_blank">订单详情</a></p>-->
<!--                    </td>-->
<!--                  </tr>-->
<!--                </table>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li>-->
<!--              <div class="order">-->
<!--                <p class="order-head"><span class="time">2014-07-05 12:37</span><span class="uid">订单号：dd2341245353</span><br/><span class="location">收货地址：玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵</span></p>-->
<!--                <table class="order-table">-->
<!--                  <tr>-->
<!--                    <td>-->
<!--                      <ul>-->
<!--                        <li>-->
<!--                          <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>-->
<!--                        </li>-->
<!--                      </ul>-->
<!--                    </td>-->
<!--                    <td>共计:<span class="total-money">&yen;233</span></td>-->
<!--                    <td>-->
<!--                      <p class="order-option"><span>用户已退订</span><br/><a href="orderDetail.php" target="_blank">订单详情</a></p>-->
<!--                    </td>-->
<!--                  </tr>-->
<!--                </table>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li>-->
<!--              <div class="order">-->
<!--                <p class="order-head"><span class="time">2014-07-05 12:37</span><span class="uid">订单号：dd2341245353</span><br/><span class="location">收货地址：玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵</span></p>-->
<!--                <table class="order-table">-->
<!--                  <tr>-->
<!--                    <td>-->
<!--                      <ul>-->
<!--                        <li>-->
<!--                          <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>-->
<!--                        </li>-->
<!--                      </ul>-->
<!--                    </td>-->
<!--                    <td>共计:<span class="total-money">&yen;233</span></td>-->
<!--                    <td>-->
<!--                      <p class="order-option"><span>请及时发货</span><br/><a class="done" href="#">已送达</a><br/><a href="orderDetail.php" target="_blank">订单详情</a></p>-->
<!--                    </td>-->
<!--                  </tr>-->
<!--                </table>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li>-->
<!--              <div class="order">-->
<!--                <p class="order-head"><span class="time">2014-07-05 12:37</span><span class="uid">订单号：dd2341245353</span><br/><span class="location">收货地址</span></p>-->
<!--                <table class="order-table">-->
<!--                  <tr>-->
<!--                    <td>-->
<!--                      <ul>-->
<!--                        <li>-->
<!--                          <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                          <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                          <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>-->
<!--                        </li>-->
<!--                      </ul>-->
<!--                    </td>-->
<!--                    <td>共计:<span class="total-money">&yen;233</span></td>-->
<!--                    <td>-->
<!--                      <p class="order-option"><span>交易完成</span><br/><a href="orderDetail.php" target="_blank">订单详情</a></p>-->
<!--                    </td>-->
<!--                  </tr>-->
<!--                </table>-->
<!--              </div>-->
<!--            </li>-->
          </ul><div class="pagination">
<!--<p> <a href="#">上一页</a><a href="#">1</a><a class="active" href="#">2</a><a href="#">3</a><a href="#">...</a><a href="#">9</a><a href="#">10</a><a href="#">下一页</a></p>-->
</div>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $(".accept").click(function(){
        var yes  = confirm("确认该订单？");
        if(!yes){
          e.cancelBubble=true;
          e.preventDefault();
          e.stopPropagation();
          return false;
        }
      });
      $(".done").click(function(){
        var yes  = confirm("确认用户已经收到货物？");
        if(!yes){
          e.cancelBubble=true;
          e.preventDefault();
          e.stopPropagation();
          return false;
        }
      });


    </script>
  </body>
</html>