<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Soulmate</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/index.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li class="active"><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="big-banner">
      <div class="slogan">
        <div class="slogan-content">
          <h1><span>我会一直陪着你<br><span class="font-flower">Soulmate</span> | 私人定制</span></h1>
          <p><a class="btn primary" href="flower.php"> <span class="text">选购鲜花</span></a></p>
        </div>
      </div>
      <div class="openhours">
        <h2>营业时间</h2>
        <p>上午 9:00 - 12:30</p>
        <p>下午 2:30 - 10:30</p>
        <div class="location">
          <p><i class="icon-location"></i> 重庆市南岸区崇文路520号</p>
        </div>
      </div>
    </div>
    <div class="content">
      <div class="started"> 
        <h2 class="title">我们的理念</h2>
        <P>Soulmate是一家重庆本地化的花店，我们希望我们的鲜花能够给在这座城市里的生活的人们带来不一样的小情趣。不同的人，不同的地点，不同的时间，让收到鲜花的人的感到惊喜的情况是不一样的。我们对每一份订单的配送的都精心安排，制造生活中的小惊喜。您可以在这里下订单给自己，或者送给其他人，我们承诺每一份鲜花的到来都是一份不一样的惊喜。</P>
      </div>
      <div class="services">
        <h2 class="title">我们的服务</h2>
        <div class="service">
          <div class="service-text">
            <h1 class="title">免费专车配送<span class="txt01">更可靠，更快速</span></h1>
            <p>所有订单由本店专车配送，同区最快20分钟送到顾客手中。</p>
          </div>
          <div class="service-image"><img src="./img/deliver.jpg" alt="送货上门"/></div>
        </div>
        <div class="service from-right">
          <div class="service-text">
            <h1 class="title">纪念日套餐<span class="txt01">意想不到的惊喜</span></h1>
            <p>购买纪念日套餐后，我们将会在您的纪念日送出特定的鲜花。可以选择通知您或者不告诉您，当您工作繁忙忘记了，我们也会给您纪念日惊喜。</p>
          </div>
          <div class="service-image"><img src="./img/rose999.jpeg" alt="纪念日图片"/></div>
        </div>
        <div class="service">
          <div class="service-text">
            <h1 class="title">现场鲜花布置<span class="txt01">用120%的态度对待</span></h1>
            <p>无论是婚礼还是生日宴席，无论是居家小客厅，还是户外大花园，我们都能给您装扮一个温馨的环境。</p>
          </div>
          <div class="service-image"><img src="./img/garden.jpeg" alt="婚礼布置"/></div>
        </div>
      </div>
       <div class="advertisement">
        <h2 class="title">了解更多</h2>
        <div class="three">
          <div class="one" data-video="./video/shipin.mp4">
            <p class="video-cover"><img src="./img/rose.jpg" alt=""/></p>
            <p class="video-text">最新活动视频-2017/02/14</p>
          </div>
          <div class="one" data-video="./video/shipin2.mp4">
            <p class="video-cover"><img src="./img/pic1.jpg" alt=""/></p>
            <p class="video-text">最新活动视频-2017/02/14</p>
          </div>
        </div>
      </div>
    </div>
      <div class="video-wrapper" style="display:none;">
      <div class="video-container">
        <div class="close-video-button" title="点击关闭视频"><i class="close-icon"></i></div>
        <video id="video-tag" src="./video/shipin2.mp4" controls="controls"></video>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
     <script>
      var video = document.getElementById('video-tag');
      $('.one').click(function(){
        video.src = $(this).data('video');
        video.play();
        $('.video-wrapper').fadeIn();
      });
      $('.close-video-button').click(function(){
        video.pause();
         $('.video-wrapper').fadeOut();
      });
      
        
    </script>
  </body>
</html>