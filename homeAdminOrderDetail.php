<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>订单详情 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['user'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <h1 class="title center">订单详情</h1>
        <table>
          <tr>
            <th>订单编号：</th>
            <td>dd15353423452</td>
            <th>订单时间：</th>
            <td>2014-05-05</td>
            <tr>
              <th>联系人:</th>
              <td>苏舒</td>
              <th>联系电话：</th>
              <td>120499234</td>
            </tr>
            <tr>
              <th>收货地址：</th>
              <td colspan="3">ffffffffffffffffffffffffffffffawe发了违法和违法和我发文</td>
            </tr>
          </tr>
          <tr>
            <th>订单物品:</th>
            <td>
              <ul>
                <li>
                  <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>
                </li>
                <li>
                  <div class="item"><span class="item-img"><img src="./img/garden.jpeg" alt="图"/></span><span class="item-name">玫瑰999朵</span><span class="item-price">&yen;199</span><span class="item-number">x2</span></div>
                </li>
              </ul>
              <th>合计：</th>
              <td><span class="total-money">&yen;246</span></td>
            </td>
          </tr>
            <tr>
              <th>是否代送：</th>
              <td>代送</td>
              <th>备注：</th>
              <td>就说感谢那天演讲的时候帮忙救场。</td>
            </tr>
          </tr>
        </table>
        <p class="center"><a class="primary btn" href="javascript:window.close();"> <span class="text">关闭本页</span></a></p>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
  </body>
</html>