<!DOCTYPE.php>
.php lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>个人中心 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/dashboard.css" rel="stylesheet"/>
  </head>
  <body>
  <?php
  session_start();
  if(empty($_SESSION['user'])){
      echo "<script language='javascript'>alert('您未登录，是否前往登录！')</script>";
      setcookie('message',"请登录");
      echo "<script>window.location='checkrose.php';</script>";
  }
  ?>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li class="active"><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
          <h3 class="title">个人中心</h3>
          <ul>
            <li><a href="homeAdminOrders.php">管理订单</a></li>
            <li><a href="homeAdminProtypes.php">品种管理</a></li>
            <li><a href="homeAdminTypes.php">类别管理</a></li>
            <li class="active"><a href="homeAdminItems.php">商品管理</a></li>
            <li><a href="homeAdminProfile.php">店铺信息</a></li>
            <li><a href="homeAdminSetting.php">账户设置</a></li>
          </ul>
        </div>
        <div class="right">
<!--          <p> -->
<!--            <select id="protype1" name="protype" placeholder="请选择品种">-->
<!--              <option value="捧花" chcked="chcked">康乃馨</option>-->
<!--              <option value="花篮">玫瑰花</option>-->
<!--              <option value="束花">百合</option>-->
<!--            </select>-->
<!--            <select id="type1" name="type" placeholder="请选择类别">-->
<!--              <option value="捧花" chcked="chcked">捧花</option>-->
<!--              <option value="花篮">花篮</option>-->
<!--              <option value="束花">束花</option>-->
<!--            </select>-->
<!--          </p>-->
<!--          <hr/>-->
          <ul class="items">
              <li>
                  <div class="item">
                      <div class="item-img" id="newitemimage"><img src="./img/new.png" alt=""/></div>
                      <div class="item-text">
                          <p> <a href="flowerAdd.php" id="newitem">新增商品</a></p>
                      </div>
                  </div>
              </li>
<!--              开始-->
              <?php
              include_once ("mysql_conn.php");
              $sql = "select * from flower ORDER BY id desc ";
              $result = (new mysql_conn())->fetchAll($sql,array(0,100));
              if (empty($result)){
                  echo "<p>当前没有鲜花信息</p>";
//                  echo "select * from flower ORDER BY id desc ";

              }else{
              foreach ($result as $row) {
                  $photo=(new mysql_conn())->fetch("select * from photo where fid = ".$row['id']);
                  $photo =( $photo== NULL ? "./img/rose999.jpeg" : $photo['addr']);
//                  $photo = strtok($row['photo'], ";") == NULL ? "./img/rose999.jpeg" : strtok($row['photo'], ";");
                  ?>
                  <li>
                      <div class="item">
                          <div class="item-img"><img src="<?php echo $photo?>" alt="tu"/></div>
                          <div class="item-text">
                              <p class="item-name"><?php echo $row['name']?></p>
                              <p class="item-price">&yen;<?php echo $row['price']?></p>
                              <p><a href="flowerChange.php?id=<?php echo $row['id']?>">修改</a>&nbsp;&nbsp;<a class="delete" href="deleteFlower.php?id=<?php echo  $row['id']?>">删除</a>
                              </p>
                          </div>
                      </div>
                  </li>
                  <?php
              }}
              ?>
<!--              结束-->

          </ul>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $("select").change(function(){
        var protype1 = $("#protype1").val();
        var type1 = $("#type1").val();
        window.location.href= "./homeAdminItems.php?protype="+protype1+"&type="+type1;
      });
      $("#newitemimage").click(function(e){
        $("#newitem").click();
      })
                
        
    </script>
  </body>
<.php>