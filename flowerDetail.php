<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>商品详情页 | Soulmate花店</title>
    <link href="./css/swiper-3.4.2.min.css" rel="stylesheet"/>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/flowerDetail.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li class="active"><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <?php
    include_once ("mysql_conn.php");
    $id = $_GET["fid"];
    $result = (new mysql_conn())->fetch("SELECT f.*,t.name `type`,p.name `protype` FROM flower f LEFT JOIN 
                type t on t.id = f.typeid LEFT JOIN protype  p on f.protypeid = p.id where f.id = ".$id);

    ?>
    <div class="main-wrapper">
      <div class="main">
        <div class="left">
          <!-- Swiper-->
          <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
                <?php
                $photoSql = "select * from photo where fid = ".$result['id'];
                $photo = (new mysql_conn())->fetchAll($photoSql,array(0,100));
                //找到图片并且读出来
//                $photoList =preg_split($result['photo'],";");
                if($photo==NULL){
                    echo  "<div class=\"swiper-slide\" style=\"background-image:url(img/rose999.jpeg);\"></div> ";
                }else{
                    foreach ($photo as $row){
                        echo "<div class=\"swiper-slide\" style=\"background-image:url(".$row['addr'].");\"></div> ";
                    }
                }
                ?>
            </div>
            <!-- Add Arrows-->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white">   </div>
          </div>
          <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                <?php
//                $keywords = preg_split("/\s/",$result['photo'] );
                if($photo==NULL){
                    echo  "<div class=\"swiper-slide\" style=\"background-image:url(img/rose999.jpeg);\"></div> ";
                }else{
                    foreach ($photo as $row){
                        echo  " <div class=\"swiper-slide\" style=\"background-image:url(".$row['addr'].");\"></div>";
                    }
                }
                ?>

            </div>
          </div>
        </div>
        <div class="right">

          <form action="addtocart.php" method="post">
            <input type="text" name="fid" value="<?php echo $result['id']?>" hidden="hidden"/>
            <h1 class="flower-name"><?php echo $result['name']?></h1>
            <h2><a class="flower-protype" href="#" title="品种"><?php echo $result['protype']?></a>·
                <a class="flower-wrapper" href="#" title="类型"><?php echo $result['type']?></a></h2>
            <div class="attribute">
              <table>
                <tr>
                  <th>花 语：</th>
                  <td><?php echo $result['say']?></td>
                </tr>
                <tr>
                  <th>材 料：</th>
                  <td><?php echo $result['meta']?></td>
                </tr>
                <tr>
                  <th>备 注：</th>
                  <td><?php echo $result['remark']?></td>
                </tr>
                <tr>
                  <th>单 价：</th>
                  <td>&yen;<span id="price"><?php echo $result['price']?></span></td>
                </tr>
                <tr class="show-icon">
                  <th>数量：</th>
                  <td><a class="icon-minus" href="javascript:;">-</a>
                    <input class="input-number" type="text" value="1" name="number"/><a class="icon-plus" href="javascript:;">+</a>(库存<?php echo $result['number']?>件)
                  </td>
                </tr>
              </table>
            </div>
            <div class="total">
              <h3>总费用为：<span class="total-money">&yen;<span id="money"><?php echo $result['price'] ?></span></span>
                <input type="submit" id="true-submit" hidden="hidden"/>
              </h3>
            </div><a class="btn primary" id="add-to-chart" href="javascript:;"><span class="text">加入购物车</span></a>
          </form>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script src="./js/swiper-3.4.2.jquery.min.js"></script>
    <script>
      var galleryTop = new Swiper('.gallery-top', {
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          spaceBetween: 10,
      });
      var galleryThumbs = new Swiper('.gallery-thumbs', {
          spaceBetween: 10,
          centeredSlides: true,
          slidesPerView: 'auto',
          touchRatio: 0.2,
          slideToClickedSlide: true
      });
      galleryTop.params.control = galleryThumbs;
      galleryThumbs.params.control = galleryTop;
      
      $(".icon-plus").on("click",function(){
        var val = Number($(".input-number").val());
        $(".input-number").val(val+1)
         $("#money").text((Number($("#price").text())*Number(val+1)).toFixed(2));
      })
      $(".icon-minus").on("click",function(){
        var val = Number($(".input-number").val());
        if(val>1){
          $(".input-number").val(val-1);
           $("#money").text((Number($("#price").text())*Number(val-1)).toFixed(2));
        }
      })
      $(".input-number").on("input",function(){
        var val = $(".input-number").val();
        if(/^\d+$/.test(val)){
            if(Number(val)<1){
              val =1;
              $(".input-number").val(1);    
            }
        }else{
          val =1;
          $(".input-number").val(1);
        }
       
        $("#money").text(Number($("#price").text())*Number(val));
      
      });
      $("#add-to-chart").on("click",function(){
        $("#true-submit").click()
      })
    </script>
  </body>
</html>