<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>购物车 | soulmate花店</title>
    <link href="./css/public.css" rel="stylesheet"/>
    <link href="./css/cart.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="header">
      <div class="logo">
        <div class="logo-content"><a class="logo-link" href="index.php" title="前往首页">Soulmate</a></div>
      </div>
      <div class="nav">
        <ul>
          <li><a class="nav-link" href="index.php" title="title">首页</a></li>
          <li><a class="nav-link" href="flower.php" title="title">分类浏览</a></li>
          <li><a class="nav-link" href="checkrose.php" title="title">个人中心</a></li>
          <li class="active"><a class="nav-link" href="cart.php" title="title">购物车</a></li>
          <li><a class="nav-link" href="contact.php" title="title">联系方式</a></li>
            <?php
            session_start();
            if(!empty($_SESSION['user'])){
                echo "<li><a class=\"nav-link\" href=\"#\" title=\"title\">".$_SESSION['user']."</a></li>";
                echo " <li><a class=\"nav-link\" href=\"destorySession.php\" title=\"title\">退出登录</a></li>";
            }else{
                ?>
                <li><a class="nav-link" href="login.php" title="title">登录</a></li>
                <li><a class="nav-link" href="regist.php" title="title">注册</a></li>
            <?php }?>
        </ul>
      </div>
    </div>
    <div class="main-wrapper">
      <div class="main">
        <form class="checkorders" action="addOrder.php" method="post">
          <div class="info">
            <h3 class="title">请确认收货信息</h3>
            <p>系统将自动输入默认收货信息， 若收货信息有误，请输入新的姓名、联系电话和详细地址。</p>
          </div>
          <table>
            <tr>
              <th>姓名：</th>
              <td>
                <input type="text" name="name" placeholder="请输入联系人姓名"/>
              </td>
            </tr>
            <tr>
              <th>联系电话：</th>
              <td>
                <input type="text" name="phone" placeholder="请输入联系人电话"/>
              </td>
            </tr>
            <tr>
              <th>详细地址：</th>
              <td>
                <input type="text" name="location" placeholder="请输入详细地址"/>
              </td>
            </tr>
          </table>
          <p class="center"><a class="basic btn" href="javascript:;" id="back"> <span class="text">返回上一步</span></a><a class="primary btn" href="javascript:;" id="order"> <span class="text">确认订单</span></a>
            <input type="submit" id="true-submit" hidden="hidden"/>
          </p>
        </form>
      </div>
    </div>
    <div class="footer">
      <div class="footer-logo"><span class="font-flower">Soulmate</span></div>
      <div class="footer-info">
        <p>致力于为平凡的生活带来新色彩.<br>详细地址：重庆市南岸区崇文路520号<br>联系电话：1888888888 <span class="copyright"> &copy;2017 soulmate花店 All rights reserved</span></p>
      </div>
    </div>
    <script src="./js/jq/jquery.min.js"></script>
    <script>
      $("#order").click(function(){
          $("#true-submit").click();
      })
      $("#back").click(function(){
        //在这里返回购物车
        window.location.href = "./cart.php";
      })

    </script>
  </body>
</html>